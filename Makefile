SOURCE = dum-ka-biryani-make-for-each-other

all:
	pdflatex $(SOURCE).tex

clean:
	rm -f *.aux *.log *.nav *.out *.pdf *.snm *.toc *.vrb *~
	rm -f examples/*~